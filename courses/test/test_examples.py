import pytest
from courses.models import Course


# A test is is a function, which name should begin with 'test'.
# If that is the case, pytest is able to find and run it:
def test_1_equal_1():
    assert 1 == 1  # This is always true of course


# Usually, a test tests a function. You want to test whether the function works as expected
def square_a_number(number):
    return number*number


def test_square_function():
    assert square_a_number(3) == 9
    assert square_a_number(5) == 25


# A test can make use of our fake database (that we have created in conftest.py)
@pytest.mark.django_db
def test_square_the_price(mock_db):
    course = Course.objects.get(name='Test course 1')  # This course is in our fake db (see conftest.py)

    assert square_a_number(course.price) == 100
