# In the pytest framework, we can create a mock-database in this file, conftest.py
# Our tests will use this database, instead of reading and writing to the actual database.
# If we do not use a fake database, soon our real database will be filled with test data!

import pytest

from courses.models import Course


@pytest.fixture()
def mock_db():
    Course.objects.create(name='Test course 1', language='NL', price=10)
    Course.objects.create(name='Test course 2', language='EN', price=20)
