import json

import pytest
from rest_framework.test import APIClient

from courses.models import Course


@pytest.mark.django_db
def test_view_data(mock_db):
    client = APIClient()
    response = client.get('/courses/', content_type='application/json')
    course_list = response.data

    # You can check whether our url emits the right types
    assert isinstance(course_list, list)
    assert isinstance(course_list[0], dict)

    # Or you can check whether our url emits the right content
    assert course_list[0]['name'] == 'Test course 1'
    assert course_list[1]['name'] == 'Test course 2'


@pytest.mark.django_db
def test_write_data(mock_db):
    client = APIClient()
    post_data = {'name': 'Test course 3', 'language': 'DU', 'price': 55}

    # Before posting a new course on our url, the count should be 2
    assert Course.objects.count() == 2

    # After posting a new course, the count should be 3
    client.post('/courses/', data=json.dumps(post_data), content_type="application/json")
    assert Course.objects.count() == 3


@pytest.mark.django_db
def test_update_a_course(mock_db):
    client = APIClient()

    course = Course.objects.get(name='Test course 1')

    # Before updating, price should be 10
    assert course.price == 10

    post_data = {'price': 250}
    # Note the changing in the URL: we now put a /1/ behind the base-url!
    client.patch('/courses/1/', data=json.dumps(post_data), content_type="application/json")

    # after updating, price should be 250
    assert Course.objects.get(name='Test course 1').price == 250


@pytest.mark.django_db
def test_delete_a_course(mock_db):
    client = APIClient()

    # before testing, count should be 2
    assert Course.objects.count() == 2

    client.delete('/courses/1/')

    assert Course.objects.count() == 1
