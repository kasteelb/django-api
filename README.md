### Django api
There is little info in this README; hopefully you have remembered how to start up a Django app.

Some keywords for doing this:
- pull
- venv
- (branch)
- migrate
- run server


#### Running tests
After intalling the requirements, you can run tests with the terminal-command:
`pytest --flake8`

You can also run them within Pycharm, which is way cooler. I will show this in the course.

#### Assignment
This weeks assignment is to go through all the motions one more time.
In the end, we want to have:
- An API url where we can (at least) GET or POST incidents. Incident is a model that has a location and an MO (modus operandi).
- One or more tests that ensure that this URL works as expected.
- After pushing to your branch, make sure the pipeline succeeds.

Also try to add some Incidents to the real database.<br>
This might be done through the admin-page.<br>
You could also practise creating Incidents by doing a POST-request on your new URL. This could be done using the Postman application, or by using the python package `requests` (or other methods of your choice).


![alt text](screenshot.png)

